<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hadits extends Model
{
    public $table = 'testimonial';
    protected $fillable = [ 'name', 'text', ];
    public $timestamps = false;
}
