<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kalender extends Model
{
    public $table = 'kalender';
    protected $fillable = [ 'title', 'image', ];
    public $timestamps = false;
}
