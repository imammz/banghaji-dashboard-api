<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $table = 'profil';
    protected $fillable = ['logo', 'title', 'description', ];
    public $timestamps = false;
}
