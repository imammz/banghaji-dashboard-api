<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public $table = 'galeri';
    protected $fillable = [ 'title', 'image', ];
    public $timestamps = false;
}
