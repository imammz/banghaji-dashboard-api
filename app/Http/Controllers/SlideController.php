<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class SlideController extends Controller
{

    public function index(){

        return Redirect::to('/admin');
    }

    public function update(Request $request){

        $destinationPath = 'images/ishaka/'; // upload path
        $request->image->move($destinationPath, $request->image->getClientOriginalName()); // uploading file to given path

        $data = Slide::find($request->id);
        $data->slide = $request->image->getClientOriginalName();
        $data->save();

        return Redirect::back()->with('success', 'Task was successful!');
    }
}
