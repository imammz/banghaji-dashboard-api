<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Gallery;
use App\Hadits;
use App\Kalender;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Profile;

class HomeController extends Controller
{
    public function index(){

        $result   = Profile::get();
        $contact  = Contact::first();
        $galeri   = Gallery::take(3)->get();
        $hadis    = Hadits::get();

        return view('index', ['result' => $result, 'contact' => $contact, 'galeri' => $galeri, 'hadis' => $hadis]);
    }

    public function kalender(){

        $contact  = Contact::first();
        $kalender = Kalender::get();

        return view('kalender', ['contact' => $contact, 'kalender' => $kalender]);
    }

    public function galeri(){

        $contact  = Contact::first();
        $galeri = Gallery::paginate(12);

        return view('galeri', ['contact' => $contact, 'galeri' => $galeri]);
    }
}
