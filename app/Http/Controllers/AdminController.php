<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminController extends Controller
{
    public function index(){

        $atas = Slide::find(1);
        $bawah = Slide::find(2);
        return view('admin/slide', compact('atas'), compact('bawah'));
    }
}
