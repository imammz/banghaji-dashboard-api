<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::auth();

    Route::get('/', 'HomeController@index');
    Route::get('/kalender', 'HomeController@kalender');
    Route::get('/galeri', 'HomeController@galeri');
});


Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'admin'], function(){

    Route::get('/', 'AdminController@index');

    Route::get('/slide', 'AdminController@index');
    Route::post('/slide', 'SlideController@update');
});