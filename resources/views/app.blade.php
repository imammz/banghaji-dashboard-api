<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pondok Pesantren Ishaka</title>
    <!-- Google Font -->
    <link href='http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Font Awesome -->


    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!-- Preloader -->
    <link rel="stylesheet" href="<?php echo asset("css/preloader.css");?>" type="text/css" media="screen, print"/>

    <!-- Icon Font-->
    <link rel="stylesheet" href="<?php echo asset("style.css");?>">
    <link rel="stylesheet" href="<?php echo asset("css/owl.carousel.css");?>">
    <link rel="stylesheet" href="<?php echo asset("css/owl.theme.default.css");?>">
    <!-- Animate CSS-->
    <link rel="stylesheet" href="<?php echo asset("css/animate.css");?>">

    <!-- Bootstrap -->
    <link href="<?php echo asset("css/bootstrap.min.css");?>" rel="stylesheet">



    <!-- Style -->
    <link href="<?php echo asset("css/style.css");?>" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="<?php echo asset("css/responsive.css");?>" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/lte-ie7.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>



<header id="HOME" style="background-position: 50% -125px;">
    <div class="section_overlay">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo asset("#");?>"><b>Pondok Pesantren Ishaka</b></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ url('/') }}">Beranda</a></li>
                        <li><a href="{{ url('/#SERVICE') }}">Profil</a></li>
                        <li><a href="{{ url('galeri') }}">Galeri Foto</a></li>
                        <li><a href="{{ url('kalender') }}">Kalender</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="home_text wow fadeInUp animated">
                        <h2>Selamat Datang</h2>
                        <p>Di Pondok Pesantren Ishaka</p>
                        <img src="<?php echo asset("images/shape.png" );?>" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="scroll_down">
                        <a href="<?php echo asset("#SERVICE");?>"><img src="<?php echo asset("images/scroll.png" );?>" alt=""></a>
                        <h4>Scroll Kebawah</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</header>


@yield('content')


<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="copyright_text   wow fadeInUp animated">
                    <p>
                        {{ $contact->alamat }}
                    </p>
                    <p style="margin-bottom: 10px;">Contact : {{ $contact->telp }}</p>
                    <p>&copy; Ishaka 2015. All Right Reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>











<!-- =========================
     SCRIPTS
============================== -->


<script src="<?php echo asset("js/jquery.min.js"); ?>"></script>
<script src="<?php echo asset("js/bootstrap.min.js"); ?>"></script>
<script src="<?php echo asset("js/jquery.nicescroll.js"); ?>"></script>
<script src="<?php echo asset("js/owl.carousel.js"); ?>"></script>
<script src="<?php echo asset("js/wow.js"); ?>"></script>
<script src="<?php echo asset("js/script.js"); ?>"></script>




</body>

</html>