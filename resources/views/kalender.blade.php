@extends('app')

@section('content')
<section class="contact" id="KALENDER">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="contact_title  wow fadeInUp animated">
                    <h1>Kalender</h1>
                    <img src="<?php echo asset("images/shape.png" );?>" alt="">
                    <p>Gambar kalender Pondok Pesantren Ishaka</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-bottom: 120px;">
        <div class="row">
            <?php
            foreach($kalender as $kal) {
            ?>
            <div class="col-md-3  wow fadeInLeft animated">
                <div class="kalender">
                    <img src="<?php echo asset("images/kalender/".$kal->image.""); ?>" alt="">
                </div>
                <p class="col-md-12 text-center"><?php echo $kal->title; ?></p>
                <p class="text-center"><a href="<?php echo asset("images/kalender/".$kal->image.""); ?>">Preview</a> </p>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</section>
@endsection