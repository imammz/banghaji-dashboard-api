@extends('app')

@section('content')

	<?php
	foreach($result as $det) {
	?>
	<div class="modal fade" id="myModal<?php echo $det->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><?php echo $det->title; ?></h4>
				</div>
				<div class="modal-body" style="text-align: justify">

					<?php echo $det->text; ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<?php
	}
	?>

	<section class="services" id="SERVICE">
		<div class="container">
			<div class="row">
				<?php
				foreach($result as $dat) {
				?>
				<div class="col-md-4 text-center">
					<div class="single_service wow fadeInUp" data-wow-delay="1s">
						<div class="i-img i-ext"><img src="<?php echo asset("images/ishaka/".$dat->logo) ?>" alt="logo"> </div>
						<h2><?php echo $dat->title; ?></h2>

						<p><?php echo $dat->description; ?></p>
						<!-- Button trigger modal -->

						<p><a href="#" data-toggle="modal" data-target="#myModal<?php echo $dat->id; ?>">Lihat Profil</a></p>
					</div>
				</div>
				<?php
				}
				?>

			</div>
		</div>
	</section>
	<section class="about_us_area" id="ABOUT">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="about_title">
						<h2>Pondok Pesantren Ishaka</h2>
						<img src="<?php echo asset("images/shape.png" );?>" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-4  wow fadeInLeft animated">
					<p class="about_us_p">Anak-anak kita adalah bagaikan kertas putih bersih; lugu apa adanya, murni tanpa rekayasa, mereka dapat tertawa atau menangis secara lepas
						sesuai apa yang dirasakan, tidak ada marah dan dendam yang berkepanjangan apalagi menahun. Hidup lepas ceria penuh dengan kegembiraan. Barangkali ini adalah
						gambaran kecil orang yang berada dalam jannah. Seiring dengan berjalannya waktu, perlahan-lahan tapi pasti mereka belajar dari lingkungan,</p>
				</div>
				<div class="col-md-4  wow fadeInRight animated">
					<p class="about_us_p"> terutama lingkungan rumah dimana orang tua berperan penting di dalamnya, oleh karena itu seandainya saat ini mereka berkata kasar, keras kepala atau susah diatur, janganlah kita cepat-cepat menyalahkan mereka, boleh jadi mereka hanya meniru-niru kelakuan buruk kita yang sengaja atau tidak sengaja kita pertunjukkan kepada mereka sehingga menjadi kebiasaannya. Rasulullah pernah mengatakan: ”setiap anak dilahirkan secara fithrah, maka orang tuanyalah dapat menjadikan mereka Yahudi, Majusi atau Nashrani”. </p>
				</div>
				<div class="col-md-4  wow fadeInRight animated">
					<p class="about_us_p">Dalam membentuk karakter yang baik pada setiap anak –menurut pakar pendidikan- paling tidak ada tiga faktor yang cukup besar mempengaruhinya : 1. Rumah, tempat tinggalnya 2. Lingkungan masyarakat, tempat mereka bermain 3. Sekolah, tempat mereka belajar (satu faktor lainnya yang barangkali terlupakan yaitu Masjid). Maka berdasarkan pendapat tersebut, alangkah baiknya bila ketiga tempat tersebut dapat dikelola dengan sebaik-baiknya, sehingga anak-anak dapat tumbuh subur,</p>
				</div>
			</div>
		</div>
	</section>


	<section class="testimonial text-center wow fadeInUp animated" id="TESTIMONIAL">
		<div class="container">
			<div class="icon">
				<i class="icon-quote"></i>
			</div>
			<div class="owl-carousel">
				@foreach($hadis as $had)
				<div class="single_testimonial text-center wow fadeInUp animated">
					<p>{{ $had->text }}</p>
					<h4>-{{ $had->name }}</h4>
				</div>
				@endforeach

			</div>
		</div>
	</section>


	<div class="fun_facts">
		<section class="header parallax home-parallax page" id="fun_facts" style="background-position: 50% -150px;">
			<div class="section_overlay">
				<div class="container">
					<div class="row">


					</div>
				</div>
			</div>
		</section>
	</div>
	<section class="work_area" id="GALLERY">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="work_title  wow fadeInUp animated">
						<h1>Galeri Foto</h1>
						<img src="<?php echo asset("images/shape.png" );?>" alt="">
						<p>Kumpulan Galeri Foto tentang kegiatan dan aktifitas yang </br>dimasukkan ke dalam Album Foto</p>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			@foreach($galeri as $gal)
				<div class="col-md-4 no_padding" style="margin-bottom: 4px;">
					<div class="single_image">
						<img src="<?php echo asset("images/ishaka/album/".$gal->image );?>" alt="">
						<div class="image_overlay">
							<a href="<?php echo asset("");?>">Detail</a>
							<h2>{{ $gal->title }}</h2>
						</div>
					</div>
				</div>
			@endforeach
			<div style="clear:both;"></div>
			<div style="margin-bottom: 50px; margin-top: 20px; text-align: center">
			<a href="{{ url('galeri') }}">View All</a>
			</div>
		</div>
	</section>


@endsection