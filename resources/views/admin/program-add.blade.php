@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Program
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/anggita/program">Program</a></li>
            <li><a href="/anggita/program-add">Tambah Program</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Program</h3>
                    </div>

                    <form role="form" action="{{ url('anggita/program') }}" method="post" >

                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputPassword2">Title</label>
                                <input type="text" class="form-control" name="title"  >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword2">Color</label>
                                <select name="color" class="form-control" style="width: 40%;">
                                    <option value="#e67e22">Orange</option>
                                    <option value="#3498db">Blue</option>
                                    <option value="#2ecc71">Green</option>
                                    <option value="#9b59b6">Purple</option>
                                    <option value="#e74c3c">Red</option>
                                </select>
                            </div>
                            <div>
                                <div class="box-header">
                                    <h3 class="box-title">Text</h3>
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                    </div><!-- /. tools -->
                                </div><!-- /.box-header -->
                                <div class="box-body pad">

                                    <textarea name="text" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"> </textarea>

                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Done </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection