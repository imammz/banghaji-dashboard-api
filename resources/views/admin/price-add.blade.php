@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Price
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/anggita/price">Price</a></li>
            <li><a href="/anggita/price-add">Add Price</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Price</h3>
                    </div>

                    <form role="form" action="{{ url('anggita/price') }}" method="post">

                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputPassword2">Package Name</label>
                                <input type="text" class="form-control" name="title"  >
                            </div>

                            <div class="form-group">
                                <label for="text">Price</label>
                                <input type="text" class="form-control" name="price" placeholder="Must be number" >
                            </div>

                            <div class="form-group">
                                <label for="text">Color</label>
                                <select name="color" class="form-control"  >
                                    <option value="price-one">Blue</option>
                                    <option value="price-two">Red</option>
                                    <option value="price-three">Orange</option>
                                    <option value="price-four">Green</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="text">Feature</label>
                                @foreach($data as $dat)
                                <div class="form-group">


                                    <input type="checkbox" name="feature[]" style="display: inline-block; margin-right: 10px;" value="{{ $dat->id }}">{{ $dat->name }}
                                    </div>
                                    @endforeach
                            </div>

                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Done </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection