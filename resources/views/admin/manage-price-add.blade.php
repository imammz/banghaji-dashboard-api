@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Manage Price
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ url('/anggita/price') }}">Price</a></li>
            <li><a href="{{ url('/anggita/manage-price') }}">Feature Price</a></li>
            <li><a href="{{ url('/anggita/manage-price/create') }}">Add Feature Price</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add Feature Price </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <form role="form" action="{{ url('anggita/manage-price') }}" method="post" >

                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword2">Name</label>
                                    <input type="text" class="form-control" name="name"  >
                                </div>

                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Done </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection