@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Price
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/anggita/price">Price</a></li>
            <li><a href="/anggita/price-edit">Edit Price</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Price</h3>
                    </div>

                    <form role="form" action="{{ url('anggita/price', $price->id) }}" method="post">

                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputPassword2">Package Name</label>
                                <input type="text" class="form-control" name="title" value="{{ $price->title }}"  >
                            </div>

                            <div class="form-group">
                                <label for="text">Price</label>
                                <input type="text" class="form-control" value="{{ $price->price }}" name="price" >
                            </div>

                            <div class="form-group">
                                <label for="text">Color</label>
                                <select name="color" class="form-control">
                                    <?php
                                    $color = [
                                            'Blue',
                                            'Red',
                                            'Orange',
                                            'Green'
                                    ];

                                    $prices = [
                                            'price-one',
                                            'price-two',
                                            'price-three',
                                            'price-four'
                                    ];
                                    ?>
                                    @for($a=0; $a<count($color); $a++)
                                    <option value="{{ $prices[$a] }}"
                                            @if($prices[$a] == $price->color)
                                            selected
                                            @endif
                                        >{{ $color[$a] }}</option>
                                    @endfor

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="text">Feature</label>
                                <?php $no = 0; ?>
                                @foreach($data as $dat)
                                    <div class="form-group">


                                        <input
                                                @for($a = 0; $a<count($price->price_desc); $a++)
                                                    @if($price->price_desc[$a]->id_price_list == $dat->id)
                                                        checked
                                                     @endif
                                               @endfor

                                               type="checkbox" name="feature[]" style="display: inline-block; margin-right: 10px;" value="{{ $dat->id }}">{{ $dat->name }}
                                    </div>
                                    <?php $no++; ?>
                                @endforeach
                            </div>

                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Done </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection