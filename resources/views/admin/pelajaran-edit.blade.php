@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Pelajaran
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/admin/pelajaran">Pelajaran</a></li>
            <li><a href="/admin/pelajaran-add">Edit Pelajaran</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->

                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('message') }}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Pelajaran</h3>
                    </div>

                    <form role="form" action="/admin/pelajaran-update" method="post">

                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="exampleInputPassword2">Title</label>
                                <input type="text" class="form-control" name="title" value="{{ $data->title }}" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Text</label>
                                <textarea class="form-control" name="text" rows="4"  >{{ $data->text }}</textarea>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection