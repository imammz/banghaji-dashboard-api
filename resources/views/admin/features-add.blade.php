@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Feature
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/anggita/features">Feature</a></li>
            <li><a href="/anggita/features-add">Add Feature</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Feature</h3>
                    </div>

                    <form role="form" action="{{ url('anggita/features') }}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputPassword2">Title</label>
                                <input type="text" class="form-control" name="title"  >
                            </div>

                            <div class="form-group">
                                <label for="text">Text</label>
                                <textarea class="form-control" name="text"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword2">Image</label>
                                <input type="file" name="image" >
                                <p style="margin-top: 10px; margin-bottom: 5px;">*Max height image 400px</p>
                                <p >*Max width image 184px</p>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Done </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection