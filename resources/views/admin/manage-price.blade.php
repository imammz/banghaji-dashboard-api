@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Manage Price
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ url('/anggita/price') }}">Price</a></li>
            <li><a href="{{ url('/anggita/manage-price') }}">Feature Price</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Feature Price list</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th style="width: 7%;">No</th>
                                <th style="width: 83%;">Name</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                            <?php $nos = 1; ?>
                            @foreach($data as $dat)

                                <tr>
                                    <td>{{ $nos++ }}</td>
                                    <td>{{ $dat->name }}</td>
                                    <td>
                                        <a style="cursor:pointer;" href="{{ url('anggita/manage-price', $dat->id) }}" ><i class="fa fa-pencil" style="margin-right: 5px;"></i></a>
                                        <a  href="/anggita/remove/price_list/{{ $dat->id }}"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>

                            @endforeach
                        </table>
                        <div class="box-footer clearfix">
                            <a href="{{ url('anggita/manage-price/create') }}" class="btn btn-primary">Add Feature Price</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection