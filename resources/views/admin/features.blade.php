@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Features
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="/anggita/features">Features</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Feature list</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th style="width: 20%;">Image</th>
                                <th style="width: 70%;">Title</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                            @foreach($data as $dat)

                                <tr>
                                    <td><div class="fet">
                                            <img src="{{ asset('images/home/'. $dat->image) }}" alt="img">
                                        </div>
                                    </td>
                                    <td>{{ $dat->title }}</td>
                                    <td>
                                        <a style="cursor:pointer;" href="{{ url('anggita/features', $dat->id) }}" ><i class="fa fa-pencil" style="margin-right: 5px;"></i></a>
                                        <a  href="/anggita/remove/features/{{ $dat->id }}"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>

                            @endforeach
                        </table>
                        <div class="box-footer clearfix">
                            <a href="{{ url('anggita/features/create') }}" class="btn btn-primary">Add Features</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection