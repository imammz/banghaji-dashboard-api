@extends('admin.index')

@section('content')

        <section class="content-header">
            <h1>
                Slide
                <small>Section</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="/admin/slide">Slide</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <!-- general form elements -->

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            <strong>Success!</strong> {{ Session::get('success') }}
                        </div>

                    @endif
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Slide Atas</h3>
                        </div>

                        <img src="{{ asset('images/ishaka/'. $atas->slide) }}" alt="image" style="height: 80px; margin: 10px;">

                        <form role="form" action="{{ url('admin/slide') }}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="box-body">

                                <input type="hidden" name="id" value="{{ $atas->id }}">

                                <div class="form-group">
                                    <label for="exampleInputPassword2">Image</label>
                                    <input type="file" name="image" >
                                    <p style="margin-top: 10px">*Maksimal size gambar adalah 1MB. jangan lebih dari 1 MB</p>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Done </button>
                            </div>
                        </form>

                        <div class="box-header with-border">
                            <h3 class="box-title">Slide Tengah</h3>
                        </div>

                        <img src="{{ asset('images/ishaka/'. $bawah->slide) }}" alt="image" style="height: 80px; margin: 10px;">

                        <form role="form" action="{{ url('admin/slide') }}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="box-body">

                                <input type="hidden" name="id" value="{{ $bawah->id }}">

                                <div class="form-group">
                                    <label for="exampleInputPassword2">Image</label>
                                    <input type="file" name="image" >
                                    <p style="margin-top: 10px">*Maksimal size gambar adalah 1MB. jika lebih dari 1MB harap di kecilkan ukuran gambarnya</p>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Done </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


@endsection