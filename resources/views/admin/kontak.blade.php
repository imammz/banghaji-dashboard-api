@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Contact
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/admin/contact">Contact</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Manage Contact</h3>
                    </div>

                    <form role="form" action="{{ url('anggita/contact') }}" method="post">

                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="exampleInputPassword2">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ $data->email }}" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword3">Phone1</label>
                                <input type="text" class="form-control" name="phone1" value="{{ $data->phone1 }}" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword3">Phone2</label>
                                <input type="text" class="form-control" name="phone2" value="{{ $data->phone2 }}" >
                            </div>


                            <div class="box-header">
                                <h3 class="box-title">Address</h3>
                                <!-- tools box -->
                                <div class="pull-right box-tools">
                                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                </div><!-- /. tools -->
                            </div><!-- /.box-header -->
                            <div class="box-body pad">

                                <textarea name="address" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"> {{ $data->address }}</textarea>

                            </div>

                        </div>

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Update">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection