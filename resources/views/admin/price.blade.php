@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Price
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="/anggita/price">Price</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Price list</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th style="width: 25%;">Title</th>
                                <th style="width: 65%;">Price</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                            @foreach($data as $dat)

                                <tr>
                                    <td>{{ $dat->title }}</td>
                                    <td>${{ $dat->price }}</td>
                                    <td>
                                        <a style="cursor:pointer;" href="{{ url('anggita/price', $dat->id) }}" ><i class="fa fa-pencil" style="margin-right: 5px;"></i></a>
                                        <a  href="{{ url('anggita/price/deletes', $dat->id) }}"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>

                            @endforeach
                        </table>
                        <div class="box-footer clearfix">
                            <a href="{{ url('anggita/price/create') }}" class="btn btn-primary" style="margin-right: 2px;">Add Price</a>
                            <a href="{{ url('anggita/manage-price') }}" class="btn btn-primary">Manage Feature Price</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection