@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Pelajaran
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/admin/pelajaran">Pelajaran</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->

                @if(Session::has('pelajaran'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('pelajaran') }}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Inbox</h3>


                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th style="width: 8%;">ID</th>
                                <th style="width: 80%;">Title</th>
                                <th style="width: 12%;">Action</th>
                            </tr>
                            @foreach($data as $dat)
                                <tr>
                                    <td>{{ $dat->id }}</td>
                                    <td>{{ $dat->title }}</td>
                                    <td>
                                        <a href="/admin/pelajaran-edit/{{ $dat->id }}"><i style="margin-right: 5px;" class="fa fa-edit"></i></a>
                                        <a href="/admin/remove/pelajaran/{{ $dat->id }}"><i class="fa fa-remove"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="box-footer clearfix">
                            {{ $data->render() }}

                                <a href="/admin/pelajaran-add" style="float: left; margin-top: 20px; margin-right: 20px;" class="btn btn-primary">Tambah Pelajaran</a>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>


@endsection