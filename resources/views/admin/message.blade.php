@extends('admin.index')

@section('content')

    <section class="content-header">
        <h1>
            Message
            <small>Section</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/admin/message">Message</a></li>
        </ol>
    </section>



    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Inbox</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            @foreach($data as $dat)
                                @if($dat->notif == 'false')
                            <tr style="background: #f2f2f2;">
                                <td>{{ $dat->name }}</td>
                                <td>{{ $dat->email }}</td>
                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dat->created_at)->toDateString()  }} (
                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dat->created_at)->diffForHumans()  }} )</td>
                                <td><a style="cursor:pointer;" onclick="modal({{ $dat->id }})"><i class="fa fa-map-marker" style="margin-right: 5px;"></i></a>
                                    <a  href="/anggita/remove/messages/{{ $dat->id }}"><i class="fa fa-remove"></i> </a>

                                    <div class="example-modal">
                                        <div class="modal" id="mod{{ $dat->id }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" onclick="closed()" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Message from : {{ $dat->email }}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>{{ $dat->message }}</p>
                                                    </div>

                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                    </div>
                                </td>
                            </tr>
                                @else
                                    <tr>
                                        <th>{{ $dat->name }}</th>
                                        <th>{{ $dat->email }}</th>
                                        <th>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dat->created_at)->toDateString()  }} (
                                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dat->created_at)->diffForHumans()  }} )</th>
                                        <th><a style="cursor:pointer;" onclick="modal({{ $dat->id }})"><i class="fa fa-map-marker" style="margin-right: 5px;"></i></a>
                                            <a  href="/admin/remove/message/{{ $dat->id }}"><i class="fa fa-remove"></i> </a>

                                            <div class="example-modal">
                                                <div class="modal" id="mod{{ $dat->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" onclick="closed()" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title">Message from : {{ $dat->email }}</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>{{ $dat->message }}</p>
                                                            </div>

                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
                                            </div>
                                        </th>
                                    </tr>
                                    @endif
                            @endforeach
                        </table>
                        <div class="box-footer clearfix">
                            {{ $data->render() }}
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>

    <script>
        function modal(id){

            $.ajax({
                url: "read/"+id,
                method : 'get',
                success: function(data){


                }
            });

            var mod = document.getElementById('mod' + id);
            mod.style.display = 'block';
        }

        function closed(){

            var all = document.getElementsByClassName('modal');

            for(var i =0; i<all.length; i++){

                all[i].style.display = 'none';
            }

            location.reload();
        }
    </script>
@endsection