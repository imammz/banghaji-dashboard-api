@extends('app')

@section('content')
    <?php
    foreach($galeri as $det) {
    ?>
    <div class="modal fade" id="myModal<?php echo $det->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $det->title; ?></h4>
                </div>
                <div class="modal-body" style="text-align: justify">

                    <img style="width: 100%;" src="<?php echo asset("images/ishaka/album/".$det->image );?>" alt="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php
    }
    ?>

<section class="work_area" id="GALLERY">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="work_title  wow fadeInUp animated">
                    <h1>Galeri Foto</h1>
                    <img src="<?php echo asset("images/shape.png" );?>" alt="">
                    <p>Kumpulan Galeri Foto tentang kegiatan dan aktifitas yang </br>dimasukkan ke dalam Album Foto</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @foreach($galeri as $gal)
            <div class="col-md-4 no_padding" style="margin-bottom: 4px;">

                <div class="single_image">
                    <img src="<?php echo asset("images/ishaka/album/".$gal->image );?>" alt="">
                    <a href="#" data-toggle="modal" data-target="#myModal<?php echo $gal->id; ?>">
                    <div class="image_overlay">

                        <h2>{{ $gal->title }}</h2>
                    </div>
                    </a>
                </div>

            </div>
        @endforeach
        <div style="margin: 0 auto; float: right;">
        {!! $galeri->render() !!}
        </div>
    </div>
</section>
    @endsection